import React, { Component } from "react";
import FoodSearch from "./FoodSearch";

import logo from "./cropped_food.jpg"

class App extends Component {
  state = {};

  render() {
	  document.body.style.backgroundColor = '#cdcdcd';
    return (

      <div className="App">
	
	<div className="styling" style={{position: "relative"}} align="center">    	
	<div style={{ zIndex: -1}} align="center">
	<div style={{zIndex: 1, bottom: 100, left: 175, bgcolor: "black"}} align="center"><h1 style={{"font-family":"monospace", "font-size":"80px"}}> Welcome to Foodie </h1></div>
		    
		    <img src={logo} alt={"logo"} style={{ width: "80%", height: "20%", border:"8px double #000000"}} align="center"/>
		    

	<div><h2 style={{"font-family":"monospace", "font-size":"30px", position:"relative", top:70}}> Enter a food item below to view nutritional information: </h2></div>	
	</div>		
	</div>
        <div className="ui text container" align="center">
          <FoodSearch />
        </div>
      </div>
    );
  }
}

export default App;
