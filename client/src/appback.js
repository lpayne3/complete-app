import React, { Component } from "react";
import FoodSearch from "./FoodSearch";

import backImage from "./largefood.png"
import logo from "./cropped_food.jpg"

class App extends Component {
  state = {};

  render() {
	  document.body.style.backgroundRepeat = "no-repeat";
	  document.body.style.backgroundSize = "1500px 2500px";
	  document.body.style.backgroundImage = "url(https://i.imgur.com/JCQNPEz.png)";
    return (

      <div className="App">
	<header className="App" align="center">
		<img src={logo} className="App-logo" alt={"logo"} style={{width: "90%", height:"15%"}}/>
		    <h1 className="App-title">Welcome to Foodie</h1>
	</header>
        <div className="ui text container" align="center">
          <FoodSearch />
        </div>
      </div>
    );
  }
}

export default App;
