const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');

const app = express();
var config = {
	host: 'localhost',
	user: 'foodview',
	password: 'F1V3Gu75',
	database: 'food_nutrition',
};

var pool = new Pool(config);

app.set('port', (process.env.PORT || 3001));
app.use(function (req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
})

const COLUMNS = [
	  "carbohydrate_g",
	  "protein_g",
	  "fa_sat_g",
	  "fa_mono_g",
	  "fa_poly_g",
	  "kcal",
	  "description"
];

if(process.env.NODE_ENV === 'production'){
	console.log('running production server');
	app.use(express.static('client/build'));
}

app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));


app.get('/api/food', async(req, res) => {
	console.log("inside api GET req");
	var param = req.query.q;
	console.log(param);
	if(!param){
		res.json({error: "Missing required Parameter"});
	}

	try{
		var response = await pool.query('select carbohydrate_g, protein_g, fa_sat_g, fa_mono_g, fa_poly_g, kcal, description from entries where description like $1 limit 100', ['%' + param+ '%']);

		console.log(JSON.stringify(response.rows));

		if(response.rowCount != 0){
			res.json(response.rows);
		}

		console.log(response);

		res.json([]);	
	}catch(e){
		console.log('error with api/food get request: '+e);
	}
});

app.listen(app.get('port'), () => {
	console.log('Complete Web App status: Running');
})


